#!/bin/bash
#
# File: agent.sh -> Build, push docker images to a registry or deploy to a Kubernetes cluster
#
# Author: Frederick Ofosu-Darko <frederick.ofosu-darko@minex360.com, fofosudarko@gmail.com>
#
# Usage: bash agent.sh BUILD_COMMAND JENKINS_JOB APP_PROJECT APP_ENVIRONMENT APP_FRAMEWORK APP_IMAGE 
#   ENV_VARS: APP_IMAGE_TAG APP_KUBERNETES_NAMESPACE APP_DB_CONNECTION_POOL
#
#

## - start here

runAs ()
{
  sudo su "$1" -c "$2"
}

addMavenWrapperProperties ()
{
  local mavenWrapperPropertiesSrc="$1"
  local mavenWrapperPropertiesDest="$2"

  runAs "$DOCKER_USER" "rsync -av $mavenWrapperPropertiesSrc $mavenWrapperPropertiesDest"
}

addSpringbootKeystores ()
{
  local dockerFile="$1"
  local keystoresSrc="$2"
  local keystoresDest="$3"

  if grep -qP 'keystores' "$dockerFile" 2> /dev/null
  then
    runAs "$DOCKER_USER" "rsync -av $keystoresSrc $keystoresDest"
  fi
}

copyDockerProject ()
{
  local jenkinsProject="$1" dockerProject="$2"

  echo $COMMAND: Copying docker project ...
  runAs "$DOCKER_USER" "
  rm -rf $dockerProject/*
  rsync -av --exclude='.git/' $jenkinsProject $dockerProject
"
}

copyDockerBuildFiles ()
{ 
  local buildFiles="$1" dockerProject="$2"

  local dockerComposeTemplate="$DOCKER_APP_CONFIG_DIR/docker-compose.template.yml"

  if [ -s "$dockerComposeTemplate" ]
  then
    dockerComposeFile="$DOCKER_APP_CONFIG_DIR/docker-compose.yml"
    formatDockerComposeTemplate "$dockerComposeTemplate" "$dockerComposeFile"
  fi

  echo $COMMAND: Copying docker build files ...
  runAs "$DOCKER_USER" "
  rsync -av --exclude='$(basename $dockerComposeTemplate)' $buildFiles $dockerProject 2> /dev/null
"
}

buildSpringbootDockerImage ()
{
  local applicationProperties="$1"
  local springbootProfile="spring.profiles.active=docker"
  local dockerFile="$DOCKER_APP_BUILD_DEST/Dockerfile"
  local targetImage="$APP_IMAGE:$APP_IMAGE_TAG"
  local imageBuildStatus=0

  echo $COMMAND: Building springboot docker image ...

  testDockerfile=$(runAs "$DOCKER_USER" "test -f $dockerFile; echo -ne \$?")

  if [[ "$testDockerfile" != "0" ]]
  then
    echo "$COMMAND: no Dockerfile found"
    exit 1
  fi

  addMavenWrapperProperties "$MAVEN_WRAPPER_PROPERTIES_SRC" "$MAVEN_WRAPPER_PROPERTIES_DEST"
  addSpringbootKeystores "$dockerFile" "$DOCKER_APP_KEYSTORES_SRC" "$DOCKER_APP_KEYSTORES_DEST"

  runAs "$DOCKER_USER" "
  sed -i 's/^spring\.profiles\.active\=.*/$springbootProfile/g' $applicationProperties
  $DOCKER_CMD build -t'$targetImage' $DOCKER_APP_BUILD_DEST
  exit \$?
" || imageBuildStatus=1

  runAs "$DOCKER_USER" "cp -a $BASE_APPLICATION_PROPERTIES $APPLICATION_PROPERTIES_DIR 2> /dev/null"

  return "$imageBuildStatus"
}

buildDockerImageFromComposeFile ()
{
  local dockerFile="$DOCKER_APP_BUILD_DEST/Dockerfile"
  local dockerComposeFile="$DOCKER_APP_BUILD_DEST/docker-compose.yml"
  local targetImage="$APP_IMAGE:$APP_IMAGE_TAG"
  local imageBuildStatus=0

  runAs "$DOCKER_USER" "
  if [[ ! -f \"$dockerFile\" ]]
  then
    echo $COMMAND: no Dockerfile found
    exit 1
  fi

  if [[ ! -f \"$dockerComposeFile\" ]]
  then
    echo $COMMAND: no docker compose file found
    exit 1
  fi

  exit 0
" || exit 1

  runAs "$DOCKER_USER" "$DOCKER_COMPOSE_CMD -f $dockerComposeFile build; exit \$?" || imageBuildStatus=1

  return "$imageBuildStatus"
}

buildAngularDockerImage ()
{
  echo $COMMAND: Building angular docker image ...
  buildDockerImageFromComposeFile
}

buildReactDockerImage ()
{
  echo $COMMAND: Building react docker image ...
  buildDockerImageFromComposeFile
}

buildDockerImage ()
{
  case "$APP_FRAMEWORK" in
    springboot)
      buildSpringbootDockerImage "$APPLICATION_PROPERTIES_DIR/application.properties"
    ;;
    angular)
      buildAngularDockerImage
    ;;
    react)
      buildReactDockerImage
    ;;
    *)
      echo $COMMAND: $APP_FRAMEWORK unknown
      exit 1
    ;;
  esac
}

pushDockerImage ()
{
  local targetImage="$APP_IMAGE:$APP_IMAGE_TAG"
  local remoteTargetImage="$CONTAINER_REGISTRY/$APP_PROJECT/$targetImage"

  echo $COMMAND: Pushing docker image ...

  runAs "$DOCKER_USER" "
  $DOCKER_CMD logout
  $DOCKER_CMD login --username '$DOCKER_LOGIN_USERNAME' --password-stdin < '$DOCKER_LOGIN_PASSWORD' '$CONTAINER_REGISTRY'
  $DOCKER_CMD tag '$targetImage' '$remoteTargetImage'
  $DOCKER_CMD push '$remoteTargetImage'
  $DOCKER_CMD logout
"
}

setKubernetesConfigs ()
{
  local kubeDir=$KUBE_HOME

  echo $COMMAND: Setting Kubernetes configs ...

  while read -r kubeConfig
  do
    KUBECONFIG="${kubeDir}/${kubeConfig}:${KUBECONFIG}"
  done < <(echo -ne "$KUBECONFIGS"| sed -e 's/:/\n/g' -e 's/$/\n/g')
}

formatDockerComposeTemplate ()
{
  if [[ -s "$1" ]] 
  then
    sed -e "s/@@APP_IMAGE@@/${APP_IMAGE}/g" \
      -e "s/@@APP_PROJECT@@/${APP_PROJECT}/g" \
      -e "s/@@CONTAINER_REGISTRY@@/${CONTAINER_REGISTRY}/g" \
      -e "s/@@APP_IMAGE_TAG@@/${APP_IMAGE_TAG}/g" "$1" 1> "$2"
  fi
}

generateKubernetesManifests ()
{
  kubernetesResourcesAnnotationsChanged ()
  {
    local changedKubernetesResourcesAnnotations=$DOCKER_APP_K8S_ANNOTATIONS_DIR/*changed*
    local originalKubernetesResourcesAnnotations=$DOCKER_APP_K8S_ANNOTATIONS_DIR/*original*
    local newEntries=$(diff \
      <(cat $changedKubernetesResourcesAnnotations 2> /dev/null| sort) \
      <(cat $originalKubernetesResourcesAnnotations 2> /dev/null| sort) | wc -l)
    
    if [[ "$newEntries" -ne 0 ]]
    then
      return 0
    fi
    
    return 1
  }

  dockerComposeFileChanged ()
  {
    if [[ "$(diff $originalComposeFile $changedComposeFile| wc -l)" -ne 0 ]]
    then
      return 0
    fi

    return 1
  }
  
  local originalComposeFile=$DOCKER_APP_COMPOSE_DIR/docker-compose.original.yml
  local changedComposeFile=$DOCKER_APP_COMPOSE_DIR/docker-compose.changed.yml
  local templateComposeFile=$DOCKER_APP_COMPOSE_DIR/docker-compose.template.yml

  echo $COMMAND: Generating kubernetes manifests ...

  formatDockerComposeTemplate "$templateComposeFile" "$changedComposeFile"

  if dockerComposeFileChanged
  then
    DOCKER_COMPOSE_FILE_CHANGED=1
  fi

  if kubernetesResourcesAnnotationsChanged
  then
    K8S_RESOURCES_ANNOTATIONS_FILES_CHANGED=1
  fi

  if [[ "$DOCKER_COMPOSE_FILE_CHANGED" -eq 0 ]] && [[ "$K8S_RESOURCES_ANNOTATIONS_FILES_CHANGED" -eq 0 ]]
  then
    return 1
  fi
  
  runAs "$DOCKER_USER" "
  getKubernetesResourcesAnnotations ()
  {
    dir -1 $DOCKER_APP_K8S_ANNOTATIONS_DIR/*changed* 2> /dev/null
  }

  getKubernetesResources ()
  {
    dir -1 $DOCKER_APP_COMPOSE_K8S_DIR/* 2> /dev/null
  }

  getKubernetesResourceFromAnnotationFile ()
  {
    echo -ne \"\$(basename \$1)\"| cut -d. -f1
  }

  getSpecLineNo ()
  {
    grep -n 'spec:' --color=never \"\$1\"|cut -d: -f1
  }

  printLinesBeforeSpec ()
  {
    sed -n \"1,\$((\$1 - 1))p\" \$2
  }

  printLinesAfterSpecInclusive ()
  {
    sed -n \"\$1,\\\$p\" \$2
  }

  selectKubernetesResource ()
  {
    echo \"\$1\"| grep --color=never \"\$2\"
  }

  addKubernetesResourceAnnotations ()
  {
    local k8sResourceAnnotations=\"\$1\"
    
    cat <<EOF             
  annotations:
\$(while read -r annotation; do echo \"    \$annotation\"; done < \$k8sResourceAnnotations)  
EOF
  }

  insertKubernetesResourceAnnotations ()
  {
    local kubernetesResource=\"\$1\"
    local kubernetesResourceAnnotations=\"\$2\"
    local kubernetesResourceCopy=\"\$kubernetesResource\".copy

    cp \$kubernetesResource \$kubernetesResourceCopy

    local specLineNo=\"\$(getSpecLineNo \$kubernetesResource)\"

    (
      printLinesBeforeSpec \"\$specLineNo\" \"\$kubernetesResourceCopy\"
      addKubernetesResourceAnnotations \"\$kubernetesResourceAnnotations\"
      printLinesAfterSpecInclusive \"\$specLineNo\" \"\$kubernetesResourceCopy\"
    ) > \$kubernetesResource || cp \$kubernetesResourceCopy \$kubernetesResource

    rm -f \$kubernetesResourceCopy
  }

  updateKubernetesResourcesWithAnnotations ()
  {
    local kubernetesResourcesAnnotations=\"\$(getKubernetesResourcesAnnotations)\"
    local kubernetesResources=\"\$(getKubernetesResources)\"

    if [[ -n \"\$kubernetesResourcesAnnotations\" ]]
    then
      for kubernetesResourceAnnotation in \"\$kubernetesResourcesAnnotations\"
      do
        annotationResource=\$(getKubernetesResourceFromAnnotationFile \"\$kubernetesResourceAnnotation\")
        kubernetesResource=\$(selectKubernetesResource \"\$kubernetesResources\" \"\$annotationResource\")
        changedResourceAnnotation=$DOCKER_APP_K8S_ANNOTATIONS_DIR/\${annotationResource}.k8s-annotations.changed
        originalResourceAnnotation=$DOCKER_APP_K8S_ANNOTATIONS_DIR/\${annotationResource}.k8s-annotations.original
        
        if [[ -n \"\$kubernetesResource\" ]]
        then
          insertKubernetesResourceAnnotations \"\$kubernetesResource\" \"\$kubernetesResourceAnnotation\"
          cp \$changedResourceAnnotation \$originalResourceAnnotation
        fi
      done
    fi

  }

  [[ -d \"$DOCKER_APP_COMPOSE_K8S_DIR\" ]] || mkdir -p \"$DOCKER_APP_COMPOSE_K8S_DIR\"
  cd $DOCKER_APP_COMPOSE_K8S_DIR && $KOMPOSE_CMD convert -f $changedComposeFile
  updateKubernetesResourcesWithAnnotations
  cp $changedComposeFile $originalComposeFile
"
  return 0
}

getKubernetesContexts ()
{
  local kubernetesContextFilter=

  case "$APP_ENVIRONMENT" in
    development|staging)
      kubernetesContextFilter=test
    ;;
    production)
      kubernetesContextFilter=prod
    ;;
    *)
      echo "$COMMAND: app environment '$APP_ENVIRONMENT' unknown"
      exit 1
    ;;
  esac
  
  runAs "$DOCKER_USER" "
  KUBECONFIG=${KUBECONFIG} $KUBECTL_CMD config get-contexts --no-headers|\
  grep $kubernetesContextFilter|\
  tr -s '[:space:]'|awk '{ print \$2; }'
"
}

deployToKubernetes ()
{
  setKubernetesConfigs
  
  generateKubernetesManifests
  
  while IFS=$'\n' read -r kubernetesContext
  do
    runAs "$ROOT_USER" "

  createKubernetesNamespaceIfNotExists ()
  {
    if ! $KUBECTL_CMD get namespaces --all-namespaces -o wide --no-headers|grep -q $APP_KUBERNETES_NAMESPACE
    then
      $KUBECTL_CMD create namespace $APP_KUBERNETES_NAMESPACE
    fi
  }

  kubernetesDeploymentsExist ()
  {
    if $KUBECTL_CMD get deployments -n $APP_KUBERNETES_NAMESPACE -l io.kompose.service=$APP_IMAGE -o wide --no-headers|grep -q $APP_IMAGE:$APP_IMAGE_TAG
    then
      return 0
    fi

    return 1
  }

  kubernetesDeploymentsScaledToZero ()
  {
    local deployments
    deployments=\$($KUBECTL_CMD get deployments -n $APP_KUBERNETES_NAMESPACE -l io.kompose.service=$APP_IMAGE -o wide --no-headers|grep $APP_IMAGE:$APP_IMAGE_TAG|awk '{ print \$2; }')

    if [[ \"\$deployments\" == \"0/0\" ]]
    then
      return 0
    fi

    return 1
  }

  export KUBECONFIG=${KUBECONFIG}

  echo $COMMAND: Switching kubernetes context to $kubernetesContext ...

  $KUBECTL_CMD config use-context $kubernetesContext

  APP_DB_CONNECTION_POOL=$APP_DB_CONNECTION_POOL

  createKubernetesNamespaceIfNotExists

  if [[ \$APP_DB_CONNECTION_POOL == 'session' ]]
  then
    echo $COMMAND: Scale kubernetes deployments to zero ...
    $KUBECTL_CMD scale deployment/$APP_IMAGE --replicas=0 -n $APP_KUBERNETES_NAMESPACE 
  fi

  if \$(kubernetesDeploymentsExist) && \
    ! \$(kubernetesDeploymentsScaledToZero) && \
    [[ \"$DOCKER_COMPOSE_FILE_CHANGED\" == \"0\" ]] && \
    [[ \"$K8S_RESOURCES_ANNOTATIONS_FILES_CHANGED\" == \"0\" ]]
  then
    echo $COMMAND: Patching kubernetes deployments ...
    eval \"$(patchKubernetesDeployment)\"
  else
    echo $COMMAND: Applying kubernetes manifests ...
    $KUBECTL_CMD apply -f $DOCKER_APP_COMPOSE_K8S_DIR -n $APP_KUBERNETES_NAMESPACE
  fi
"
  done < <(getKubernetesContexts)

  echo $COMMAND: Kubernetes manifests deployed
}

getKubernetesPatchDeploymentSpec ()
{
  cat <<EOF
spec:
  template:
    metadata:
      creationTimestamp: '$(date +'%FT%H:%M:%SZ')'
EOF
}

patchKubernetesDeployment ()
{
  local KUBERNETES_DEPLOYMENT_PATCH=$(getKubernetesPatchDeploymentSpec)
  cat <<EOF
  $KUBECTL_CMD patch -n $APP_KUBERNETES_NAMESPACE -f $DOCKER_APP_COMPOSE_K8S_DIR/*deployment* --patch '$KUBERNETES_DEPLOYMENT_PATCH'
EOF
}

abortBuildProcess ()
{
  echo $COMMAND: docker image build process aborted.
  exit 1
}

getAppImageTag ()
{
  local appImageTag=${APP_IMAGE_TAG}

  if [[ "$USE_GIT_COMMIT" = "true" ]]
  then
    if [[ -n "$GIT_COMMIT" ]]
    then
      appImageTag=$(echo -ne $GIT_COMMIT|cut -c1-10)
    fi
  fi

  case "$APP_ENVIRONMENT"
  in
    development) appImageTag="dev-${appImageTag}";;
    staging) appImageTag="staging-${BUILD_DATE}-${appImageTag}";;
    production) appImageTag="prod-${BUILD_DATE}-${appImageTag}";;
    *)
      echo >&2 "$COMMAND: app environment '$APP_ENVIRONMENT' unknown"
      exit 1
    ;;
  esac

  printf '%s' $appImageTag
}

createDefaultDirectoriesIfNotExists ()
{
  runAs "$DOCKER_USER" "
  defaultDirs=(
    ${DOCKER_APP_BUILD_DEST}
    ${DOCKER_APP_CONFIG_DIR}
    ${DOCKER_APP_COMPOSE_DIR}
    ${DOCKER_APP_COMPOSE_K8S_DIR}
    ${DOCKER_APP_KEYSTORES_SRC}
    ${MAVEN_WRAPPER_PROPERTIES_SRC}
    ${DOCKER_APP_K8S_ANNOTATIONS_DIR}
  )

  for defaultDir in \${defaultDirs[@]}
  do
    [[ ! -d \"\$defaultDir\" ]] && mkdir -p \"\$defaultDir\"
  done
"
}

COMMAND="$0"

if [[ "$#" -ne 6 ]]
then
  echo -ne "$COMMAND: expects 5 arguments i.e. BUILD_COMMAND "
  echo -ne "JENKINS_JOBS APP_PROJECT APP_FRAMEWORK APP_IMAGE"
  exit 1
fi

BUILD_COMMAND="${1:-build}"
JENKINS_JOB="$2"
APP_PROJECT="$3"
APP_ENVIRONMENT="$4"
APP_FRAMEWORK="$5"
APP_IMAGE="$6"

APP_IMAGE_TAG="${APP_IMAGE_TAG:-latest}"
APP_KUBERNETES_NAMESPACE=${APP_KUBERNETES_NAMESPACE:-default}
APP_DB_CONNECTION_POOL="${APP_DB_CONNECTION_POOL:-transaction}"

: ${DOCKER_CMD="$(which docker)"}
: ${DOCKER_COMPOSE_CMD="$(which docker-compose)"}
: ${KOMPOSE_CMD="$(which kompose)"}
: ${KUBECTL_CMD="$(which kubectl)"}
: ${CONTAINER_REGISTRY=${CONTAINER_REGISTRY:-containers.minex360.com}}
: ${BUILD_COMMANDS='^(build|build\-push|build\-push\-deploy|push|deploy)$'}
: ${APP_ENVIRONMENTS='^(development|staging|production)$'}
: ${JENKINS_WORKSPACE='/var/lib/jenkins/workspace'}
: ${DOCKER_USER='docker'}
: ${JENKINS_USER='jenkins'}
: ${ROOT_USER='root'}
: ${DOCKER_HOME='/home/docker'}
: ${DOCKER_APPS_DIR="$DOCKER_HOME/apps"}
: ${DOCKER_APPS_CONFIG_DIR="$DOCKER_HOME/config"}
: ${DOCKER_APPS_KEYSTORES_DIR="$DOCKER_HOME/keystores"}
: ${DOCKER_APPS_COMPOSE_DIR="$DOCKER_HOME/compose"}
: ${DOCKER_APPS_KUBERNETES_ANNOTATIONS_DIR="$DOCKER_HOME/k8s-annotations"}
: ${DOCKER_APPS_KUBERNETES_DIR="$DOCKER_HOME/.kube"}
: ${DOCKER_APP_BUILD_SRC="$JENKINS_WORKSPACE/$JENKINS_JOB"}
: ${DOCKER_APP_BUILD_DEST="$DOCKER_APPS_DIR/$APP_FRAMEWORK/$APP_ENVIRONMENT/$JENKINS_JOB"}
: ${DOCKER_APP_CONFIG_DIR="$DOCKER_APPS_CONFIG_DIR/$APP_FRAMEWORK/$APP_ENVIRONMENT/$JENKINS_JOB"}
: ${DOCKER_APP_COMPOSE_DIR="$DOCKER_APPS_COMPOSE_DIR/$APP_FRAMEWORK/$APP_ENVIRONMENT/$JENKINS_JOB"}
: ${DOCKER_APP_COMPOSE_K8S_DIR="$DOCKER_APP_COMPOSE_DIR/kubernetes"}
: ${DOCKER_APP_K8S_ANNOTATIONS_DIR="$DOCKER_APPS_KUBERNETES_ANNOTATIONS_DIR/$APP_FRAMEWORK/$APP_ENVIRONMENT/$JENKINS_JOB"}
: ${DOCKER_APP_BUILD_FILES="$DOCKER_APP_CONFIG_DIR/*{Dockerfile,docker-compose}*"}
: ${DOCKER_LOGIN_USERNAME='builder-script'}
: ${DOCKER_LOGIN_PASSWORD="$DOCKER_HOME/.secrets/${DOCKER_LOGIN_USERNAME}"}
: ${DOCKER_APP_KEYSTORES_SRC="/home/docker/keystores/$APP_PROJECT/$APP_ENVIRONMENT/keystores"}
: ${DOCKER_APP_KEYSTORES_DEST="$DOCKER_APP_BUILD_DEST"}
: ${MAVEN_WRAPPER_PROPERTIES_SRC="${DOCKER_APPS_CONFIG_DIR}/${APP_FRAMEWORK}/maven-wrapper/"}
: ${MAVEN_WRAPPER_PROPERTIES_DEST="$DOCKER_APP_BUILD_DEST"}
: ${BUILD_DATE=`date +'%Y%m%d'`}
: ${KUBE_HOME=${KUBE_HOME:-${DOCKER_APPS_KUBERNETES_DIR}}}

KUBECONFIGS_INITIAL=smartsapp-k8s-test-cluster-config
KUBECONFIGS=${KUBECONFIGS:-${KUBECONFIGS_INITIAL}}
KUBECONFIG=
DOCKER_COMPOSE_FILE_CHANGED=0
K8S_RESOURCES_ANNOTATIONS_FILES_CHANGED=0
APP_IMAGE_TAG=$(getAppImageTag)

# test build commands
[[ -x "$DOCKER_CMD" ]] || exit 1
[[ -x "$DOCKER_COMPOSE_CMD" ]] || exit 1
[[ -x "$KOMPOSE_CMD" ]] || exit 1
[[ -x "$KUBECTL_CMD" ]] || exit 1

# check passed build command
if ! echo -ne "$BUILD_COMMAND"| grep -qP "$BUILD_COMMANDS"
then
  echo "$COMMAND: build command must be in $BUILD_COMMANDS"
  exit 1
fi

# check passed app environment
if ! echo -ne "$APP_ENVIRONMENT"| grep -qP "$APP_ENVIRONMENTS"
then
  echo "$COMMAND: app environment must be in $APP_ENVIRONMENTS"
  exit 1
fi

createDefaultDirectoriesIfNotExists
copyDockerProject "$DOCKER_APP_BUILD_SRC" "$(dirname $DOCKER_APP_BUILD_DEST)"
copyDockerBuildFiles "$DOCKER_APP_BUILD_FILES" "$DOCKER_APP_BUILD_DEST"

case "$APP_FRAMEWORK" in
  springboot)
    APPLICATION_PROPERTIES_DIR="$DOCKER_APP_BUILD_DEST/src/main/resources"
    BASE_APPLICATION_PROPERTIES="$DOCKER_APP_CONFIG_DIR/application.properties"
    APPLICATION_PROPERTIES="$DOCKER_APP_CONFIG_DIR/application-docker.properties"

    runAs "$DOCKER_USER" "
      test -d $APPLICATION_PROPERTIES_DIR || mkdir -p $APPLICATION_PROPERTIES_DIR
      cp $BASE_APPLICATION_PROPERTIES $APPLICATION_PROPERTIES_DIR
      cp $APPLICATION_PROPERTIES $APPLICATION_PROPERTIES_DIR
    "
  ;;
  angular|react)
    :
  ;;
  *)
    echo "$COMMAND: app framework '$APP_FRAMEWORK' unknown"
    exit 1
  ;;
esac

if [[ "$BUILD_COMMAND" == "build" ]]
then
  buildDockerImage || abortBuildProcess
elif [[ "$BUILD_COMMAND" == "build-push" ]]
then
  buildDockerImage || abortBuildProcess
  pushDockerImage
elif [[ "$BUILD_COMMAND" == "build-push-deploy" ]]
then
  buildDockerImage || abortBuildProcess
  pushDockerImage
  deployToKubernetes
elif [[ "$BUILD_COMMAND" == "push" ]]
then
  pushDockerImage
elif [[ "$BUILD_COMMAND" == "deploy" ]]
then
  deployToKubernetes
else
  echo "$COMMAND: no build command specified"
  exit 1
fi

exit 0

## -- finish